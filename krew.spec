%define debug_package %{nil}

Name:           krew
Version:        0.4.4
Release:        0%{?dist}
Summary:        Find and install kubectl plugins

License:        ASL 2.0
URL:            https://krew.sigs.k8s.io/
Source0:        https://github.com/kubernetes-sigs/%{name}/releases/download/v%{version}/%{name}-linux_amd64.tar.gz

%description
Find and install kubectl plugins

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name}-linux_amd64 %{buildroot}/usr/bin/krew

%files
%license LICENSE
/usr/bin/krew

%changelog
* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.4.4

* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM